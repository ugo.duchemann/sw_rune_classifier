#include "parser.hh"
#include "rune.hh"
#include "pretty-printer.hh"

#include <fstream>
#include <iostream>
#include <typeinfo>

namespace parser{

    void parser(std::string file)
    {
        std::fstream f;
        f.open(file.c_str());
        std::string line;
        std::string::size_type cp = 0;
        if (f.is_open())
        {
            auto runes = new std::vector<rune::rune*>();
            getline(f,line);
            while(getline(f,line))
            {
                cp = line.find_first_of(',');
                std::vector<std::string> car;
                while(cp != std::string::npos)
                {
                    car.push_back(line.substr(0, cp));
                    line = line.substr(cp+1);
                    cp = line.find_first_of(",");
                }
                car.push_back(line.substr(0, cp));
                auto r = new rune::rune(car);
                runes->push_back(r);
                std::cout << *r << std::endl;
            }
        }
        f.close();
    }
}
