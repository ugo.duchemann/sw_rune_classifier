#include <iostream>
#include "pretty-printer.hh"

std::ostream& operator<<(std::ostream& ostr, rune::stat const& s)
{
    if (s.effect_get() == rune::effect::NUL)
    {
        ostr << " -- ";
        return ostr;
    }
    ostr << s.effect_get() << '(';
    if (s.flat_get())
        ostr << '+';
    ostr << s.value_get();
    if (!s.flat_get())
        ostr << '%';
    ostr << ')';
    return ostr;
}

std::ostream& operator<<(std::ostream& ostr,rune::rune& r)
{
    ostr << r.id_get() << ' ' << r.id_monster_get() << ' ' << r.set_get() << '_'
        << r.slot_get() << ' ' << r.stars_get() << "* +" << r.lvl_get() << ' ' << r.eff_get() << "% \n"
        << r.primary_get() << ' ' << r.prefix_get() << ' ' << r.first_get() << ' ' 
        << r.second_get() << ' ' << r.third_get() << ' ' << r.fourth_get() << std::endl;
    return ostr;
}

std::ostream& operator<<(std::ostream& ostr, rune::set s)
{
    if (s == rune::set::Energy)
        ostr << "Energy"; 
    else if (s == rune::set::Guard)
        ostr << "Guard";
    else if (s == rune::set::Swift)
        ostr << "Swift";
    else if (s == rune::set::Blade)
        ostr << "Blade";
    else if (s == rune::set::Rage)
        ostr << "Rage";
    else if (s == rune::set::Focus)
        ostr << "Focus";
    else if (s == rune::set::Endure)
        ostr << "Endure";
    else if (s == rune::set::Fatal)
        ostr << "Fatal";
    else if (s == rune::set::Despair)
        ostr << "Despair";
    else if (s == rune::set::Vampire)
        ostr << "Vampire";
    else if (s == rune::set::Violent)
        ostr << "Violent";
    else if (s == rune::set::Nemesis)
        ostr << "Nemesis";
    else if (s == rune::set::Will)
        ostr << "Will";
    else if (s == rune::set::Shield)
        ostr << "Shield";
    else if (s == rune::set::Revenge)
        ostr << "Revenge";
    else if (s == rune::set::Destroy)
        ostr << "Destroy";
    else if (s == rune::set::Fight)
        ostr << "Fight";
    else if (s == rune::set::Determination)
        ostr << "Determination";
    else if (s == rune::set::Enhance)
        ostr << "Enhance";
    else if (s == rune::set::Accuracy)
        ostr << "Accuracy";
    else if (s == rune::set::Tolerance)
        ostr << "Tolerance";
    else
        ostr << "Unknown rune set.";

    return ostr;
}

std::ostream& operator<<(std::ostream& ostr, rune::effect s)
{
    if (s == rune::effect::HP)
        ostr << "HP";
    else if (s == rune::effect::ATK)
        ostr << "ATK";
    else if (s == rune::effect::DEF)
        ostr << "DEF";
    else if (s == rune::effect::SPD)
        ostr << "SPD";
    else if (s == rune::effect::CR)
        ostr << "CR";
    else if (s == rune::effect::CD)
        ostr << "CD";
    else if (s == rune::effect::RES)
        ostr << "RES";
    else if (s == rune::effect::ACC)
        ostr << "ACC";
    else
        ostr << "Unknown effect.";
    return ostr;
}
