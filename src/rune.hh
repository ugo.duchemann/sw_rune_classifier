#pragma once
#include <vector>
#include <string>

namespace rune
{
    enum set
    {
        Energy,
        Guard,
        Swift,
        Blade,
        Rage,
        Focus,
        Endure,
        Fatal,
        Despair,
        Vampire,
        Violent,
        Nemesis,
        Will,
        Shield,
        Revenge,
        Destroy,
        Fight,
        Determination,
        Enhance,
        Accuracy,
        Tolerance,
        ukn
    };

    enum effect
    {
        HP,
        ATK,
        DEF,
        SPD,
        CR,
        CD,
        RES,
        ACC,
        NUL
    };

    class stat
    {
        private:
            effect effect_;
            int value_;
            bool flat_;
        public:
            stat(effect,int,bool);
            stat(std::string);
            void effect_set(effect);
            void value_set(int);
            void flat_set(bool);

            effect effect_get() const;
            int value_get() const;
            bool flat_get() const;

    };

    class rune{
        private:
            int id_;
            std::string id_monster_;
            set set_;
            int slot_;
            int stars_;
            int lvl_;
            int sell_price_;
            stat primary_;
            stat prefix_;
            stat first_;
            stat second_;
            stat third_;
            stat fourth_;
            int eff_;
        public:
//            rune(int,std::string,set,int,int,int,stat,stat,stat,stat,stat,stat,int);
            rune(std::vector<std::string>);
            int id_get() const;
            std::string id_monster_get() const;
            set set_get() const;
            int slot_get() const;
            int stars_get() const;
            int lvl_get() const;
            int sell_price_get() const;
            stat primary_get() const;
            stat prefix_get() const;
            stat first_get() const;
            stat second_get() const;
            stat third_get() const;
            stat fourth_get() const;
            int eff_get() const;

            void id_set(int);
            void id_monster_set(std::string);
            void set_set(set);
            void slot_set(int);
            void stars_set(int);
            void lvl_set(int);
            void sell_price_set(int);
            void primary_set(stat);
            void prefix_set(stat);
            void first_set(stat);
            void second_set(stat);
            void third_set(stat);
            void fourth_set(stat);
            void eff_set(int);
    };
}
