#include "rune.hh"
#include <iostream>

namespace rune
{
    /*    rune::rune(int id, std::string id_m, set set, int slot, int stars, int sp, stat primary, stat pref, stat first, stat second, stat third, stat fourth, int eff)
          :id_(id), id_monster_(id_m), set_(set), slot_(slot), stars_(stars), sell_price_(sp), primary_(primary), prefix_(pref), first_(first), second_(second), third_(third), fourth_(fourth), eff_(eff)
          {}*/
    rune::rune(std::vector<std::string> v)
        :primary_(stat(v[7])),prefix_(stat(v[8])),first_(stat(v[9])),second_(stat(v[10])),third_(stat(v[11])),fourth_(stat(v[12]))
    {
        id_ = std::stoi(v[0]);
        id_monster_ = v[1];
        slot_ = std::stoi(v[3]);
        stars_ = std::stoi(v[4]);
        lvl_ = std::stoi(v[5]);
        sell_price_ = std::stoi(v[6]);
        eff_ = std::stoi(v[13]);
        if (v[2] == "Energy")
            set_ = set::Energy;
        else if (v[2] == "Guard")
            set_ = set::Guard;
        else if (v[2] == "Swift")
            set_ = set::Swift;
        else if (v[2] == "Blade")
            set_ = set::Blade;
        else if (v[2] == "Rage")
            set_ = set::Rage;
        else if (v[2] == "Focus")
            set_ = set::Focus;
        else if (v[2] == "Endure")
            set_ = set::Endure;
        else if (v[2] == "Fatal")
            set_ = set::Fatal;
        else if (v[2] == "Despair")
            set_ = set::Despair;
        else if (v[2] == "Vampire")
            set_ = set::Vampire;
        else if (v[2] == "Violent")
            set_ = set::Violent;
        else if (v[2] == "Nemesis")
            set_ = set::Nemesis;
        else if (v[2] == "Will")
            set_ = set::Will;
        else if (v[2] == "Shield")
            set_ = set::Shield;
        else if (v[2] == "Revenge")
            set_ = set::Revenge;
        else if (v[2] == "Destroy")
            set_ = set::Destroy;
        else if (v[2] == "Fight")
            set_ = set::Fight;
        else if (v[2] == "Determination")
            set_ = set::Determination;
        else if (v[2] == "Enhance")
            set_ = set::Enhance;
        else if (v[2] == "Accuracy")
            set_ = set::Accuracy;
        else if (v[2] == "Tolerance")
            set_ = set::Tolerance;
        else
            set_ = set::ukn;

    }

    int rune::id_get() const {return id_;}
    std::string rune::id_monster_get() const{return id_monster_;}
    set rune::set_get() const{return set_;}
    int rune::slot_get() const{return slot_;}
    int rune::stars_get() const{return stars_;}
    int rune::lvl_get() const{return lvl_;}
    int rune::sell_price_get() const{return sell_price_;}
    stat rune::primary_get() const{return primary_;}
    stat rune::prefix_get() const{return prefix_;}
    stat rune::first_get() const{return first_;}
    stat rune::second_get() const{return second_;}
    stat rune::third_get() const{return third_;}
    stat rune::fourth_get() const{return fourth_;}
    int rune::eff_get() const{return eff_;}

    void rune::id_set(int id){id_ = id;}
    void rune::id_monster_set(std::string idm){id_monster_ = idm;}
    void rune::set_set(set s){set_ = s;}
    void rune::slot_set(int slot){slot_ = slot;}
    void rune::stars_set(int stars){stars_ = stars;}
    void rune::lvl_set(int lvl){lvl_ = lvl;}
    void rune::sell_price_set(int sp){sell_price_ = sp;}
    void rune::primary_set(stat s){primary_ = s;}
    void rune::prefix_set(stat s){prefix_ = s;}
    void rune::first_set(stat s){first_ = s;}
    void rune::second_set(stat s){second_ = s;}
    void rune::third_set(stat s){third_ = s;}
    void rune::fourth_set(stat s){fourth_ = s;}
    void rune::eff_set(int eff){eff_ = eff;}

    stat::stat(effect ef, int v, bool f)
        :effect_(ef), value_(v), flat_(f)
    {}
    stat::stat(std::string s)
    {
        if (s == "")
        {
            flat_ = 1;
            value_ = 0;
            effect_ = effect::NUL;
            return;
        }
        std::string::size_type pos = s.find_last_of(' ');
        if (s.substr(pos+1) == "(Converted)")
        {
            s = s.substr(0, pos);
            pos = s.find_last_of(' ');
        }
        std::string effet = s.substr(0, pos);
        if (effet == "HP")
            effect_ = effect::HP;
        else if (effet == "ATK")
            effect_ = effect::ATK;
        else if (effet == "DEF")
            effect_ = effect::DEF;
        else if (effet == "SPD")
            effect_ = effect::SPD;
        else if (effet == "CRI Rate")
            effect_ = effect::CR;
        else if (effet == "CRI Dmg")
            effect_ = effect::CD;
        else if (effet == "Resistance")
            effect_ = effect::RES;
        else if (effet == "Accuracy")
            effect_ = effect::ACC;
        std::string str = s.substr(pos+1);
        if (str[0] == '+')
        {
            flat_ = 1;
            std::string str2 = str.substr(1);
            value_ = std::stoi(str2);
        }
        else{
            flat_ = 0;
            std::string str2 = str.substr(0, str.size()-1);
            value_ = std::stoi(str2);
        }
    }

    void stat::effect_set(effect eff){effect_ = eff;}
    void stat::value_set(int v){value_ = v;}
    void stat::flat_set(bool f){flat_ = f;}

    effect stat::effect_get() const{return effect_;}
    int stat::value_get() const{ return value_;}
    bool stat::flat_get() const{return flat_;}
}
