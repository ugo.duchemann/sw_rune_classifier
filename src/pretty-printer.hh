#include "rune.hh"
#include <iostream>

std::ostream& operator<<(std::ostream&, rune::stat&);
std::ostream& operator<<(std::ostream&, rune::rune&);
std::ostream& operator<<(std::ostream& ostr, rune::set);
std::ostream& operator<<(std::ostream& ostr, rune::effect);
