CXX=g++
CXXFLAGS= -Wall -Wextra -pedantic -std=c++14 -g

SRC=src/main.cc src/parser.cc src/pretty-printer.cc src/rune.cc
OBJS=$(SRC:.cc=.o)
EXEC=sw_rune_manager

.PHONY: all clean check

all:$(OBJS)
	$(CXX) $(CXXFLAGS) -o $(EXEC) $(OBJS)

check:
	./tests/main.sh

clean:
	$(RM) $(EXEC) $(OBJS)
